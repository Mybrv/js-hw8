//1. Створіть масив з рядків "travel", "hello", "eat", "ski", "lift" та обчисліть кількість рядків з довжиною більше
// за 3 символи. Вивести це число в консоль.

const arr = ["travel", "hello", "eat", "ski", "lift" ];

arr.forEach((item) => {
    if (item.length > 3) {
        console.log(item)
    }
})

//2. Створіть масив із 4 об'єктів, які містять інформацію про людей: {name: "Іван", age: 25, sex: "чоловіча"}. Наповніть
// різними даними. Відфільтруйте його, щоб отримати тільки об'єкти зі sex "чоловіча".
// Відфільтрований масив виведіть в консоль.

const arrOfUser = [
    {name: "Іван", age: 25, sex: "чоловіча"},
    {name: "Mike", age: 25, sex: "чоловіча"},
    {name: "Petro", age: 25, sex: "чоловіча"},
    {name: "Marry", age: 25, sex: "жіноча"}
]

let arrFiltered = arrOfUser.filter(arrOfUser => arrOfUser.sex === "чоловіча")

console.log(arrFiltered)

//3. Реалізувати функцію фільтру масиву за вказаним типом даних. (Опціональне завдання)
// Технічні вимоги:
// - Написати функцію filterBy(), яка прийматиме 2 аргументи. Перший аргумент - масив, який міститиме будь-які дані,
// другий аргумент - тип даних.
// - Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент, за винятком тих, тип
// яких був переданий другим аргументом. Тобто якщо передати масив ['hello', 'world', 23, '23', null], і другим
// аргументом передати 'string', то функція поверне масив [23, null].

const arrOfDataType = ['hello', 'world', 23, '23', null, {}];

function filterBy(array, datatype) {
   return array.filter(item => typeof item !== datatype || item === null)
}

const filterarr = filterBy(arrOfDataType, 'string')

console.log(filterarr);